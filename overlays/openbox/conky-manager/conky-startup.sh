killall conky
cd ~/conky-manager/themes/Gold\&Grey
conky -c ~/conky-manager/themes/Gold\&Grey/config/cpu &
conky -c ~/conky-manager/themes/Gold\&Grey/config/disk &
conky -c ~/conky-manager/themes/Gold\&Grey/config/mem &
conky -c ~/conky-manager/themes/Gold\&Grey/config/net &
conky -c ~/conky-manager/themes/Gold\&Grey/config/time &
