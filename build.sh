#!/bin/bash

iso_name=instarch
iso_label="INSTARCH_$(date +%Y%m)"
iso_version=$(date +%Y.%m.%d)
install_dir=arch

script_path=$(readlink -f ${0%/*})
out_dir=${script_path}/out

install_kde=0
install_openbox=0
install_heavy=0
enable_testing=0
desktop=i3
build_arch=""

while true; do
    case "$1" in
        -64)
            build_arch="${build_arch} x86_64"
            shift
            ;;
        -32)
            build_arch="${build_arch} i686"
            shift
            ;;
        -ff)
            rm -rf work/
            shift
            ;;
        -f)
            rm work/*/build.*
            shift
            ;;
        -h)
            install_heavy=1
            shift
            ;;
        -k)
            install_kde=1
            desktop=kde-plasma
            shift
            ;;
        -o)
            install_openbox=1
            desktop=openbox
            shift
            ;;
        -t)
            enable_testing=1
            shift
            ;;
        -d)
            desktop=$2
            shift 2
            ;;
        *)
            break
            ;;
    esac
done

# default architectures
[[ "${build_arch}" == "" ]] && build_arch="x86_64 i686"

set -e -u

# tweak ISO name based on the creation flags
[ ${install_kde}     -eq 1 ] && iso_name="${iso_name}-kde"
[ ${install_openbox} -eq 1 ] && iso_name="${iso_name}-openbox"
[ ${install_heavy}   -eq 1 ] && iso_name="${iso_name}-big"
[ ${enable_testing}  -eq 1 ] && iso_name="${iso_name}-testing"

# Helper function to run make_*() only one time per architecture.
run_once() {
    if [[ ! -e ${work_dir}/build.${1}_${arch} ]]; then
        $1
        touch ${work_dir}/build.${1}_${arch}
    fi
}

# Setup custom pacman.conf with current cache directories.
make_pacman_conf() {
    local _cache_dirs
    _cache_dirs=($(pacman -v 2>&1 | grep '^Cache Dirs:' | sed 's/Cache Dirs:\s*//g'))
    sed -r "s|^#?\\s*CacheDir.+|CacheDir = $(echo -n ${_cache_dirs[@]})|g" ${script_path}/pacman.conf > ${pacman_conf}

    if [[ "${arch}" == "x86_64" ]]; then
        sed -i '/\[multilib\]/,+2s/^#//' ${pacman_conf}
        if [ ${enable_testing} -eq 1 ]; then
            sed -i '/\[multilib-testing\]/,+2s/^#//' ${pacman_conf}
        fi
    fi

    if [ ${enable_testing} -eq 1 ]; then
        echo "--------------------------- ENABLING TESTING REPOS ---------------------------"
        sed -i -e '/\[testing\]$/,+2s/^#//' \
               -e '/\[community-testing\]/,+2s/^#//' ${pacman_conf}
    fi
}

# Base installation (root-image)
make_basefs() {
    ${_pre} init
}

# Additional packages (root-image)
make_packages() {
    if [ ${install_heavy} -eq 1 ]; then
        ${_pre} -p "$(grep -h -v ^# ${script_path}/packages.heavy)" install
    else
        ${_pre} -p "ttf-ubuntu-font-family" install
    fi

    ${_pre} -p "$(grep -h -v ^# ${script_path}/packages.core)" install

    if [ ${install_kde} -eq 1 ]; then
        _kde=kdebase
        if [ ${install_heavy} -eq 1 ]; then
            _kde=${_kde}-workspace
        fi

        ${_pre} -p "${_kde}" install
    fi

    if [ ${install_openbox} -eq 1 ]; then
        ${_pre} -p "instarch-overlay-openbox-git" install
    fi
}
# Copy mkinitcpio archiso hooks and build initramfs (root-image)
make_setup_mkinitcpio() {
    cp /usr/lib/initcpio/hooks/archiso ${work_dir}/root-image/usr/lib/initcpio/hooks
    cp /usr/lib/initcpio/install/archiso ${work_dir}/root-image/usr/lib/initcpio/install
    cp ${script_path}/mkinitcpio.conf ${work_dir}/root-image/etc/mkinitcpio-archiso.conf

    # force linux to be re-installed to generate images
    pacman -Sy linux -r ${work_dir}/root-image --config ${pacman_conf} --noconfirm

    ${_pre} -r 'mkinitcpio -c /etc/mkinitcpio-archiso.conf -k /boot/vmlinuz-linux -g /boot/archiso.img' run
}

# Customize installation (root-image)
make_customize_root_image() {
    cp -af ${script_path}/root-image ${work_dir}/

    ${_pre} -r "/root/customize_root_image.sh ${desktop}" run
    rm ${work_dir}/root-image/root/customize_root_image.sh

    cp ${pacman_conf} ${work_dir}/root-image/etc/
}

# Prepare ${install_dir}/boot/
make_boot() {
    mkdir -p ${work_dir}/iso/${install_dir}/boot/${arch}
    cp ${work_dir}/root-image/boot/archiso.img ${work_dir}/iso/${install_dir}/boot/${arch}/archiso.img
    cp ${work_dir}/root-image/boot/vmlinuz-linux ${work_dir}/iso/${install_dir}/boot/${arch}/vmlinuz
}

# Prepare /${install_dir}/boot/syslinux
make_syslinux() {
    mkdir -p ${work_dir}/iso/${install_dir}/boot/syslinux
    sed "s|%ARCHISO_LABEL%|${iso_label}|g;
         s|%INSTALL_DIR%|${install_dir}|g;
         s|%VERSION%|${iso_version}|g;
         s|%ARCH%|${arch}|g" ${script_path}/syslinux/syslinux.cfg > ${work_dir}/iso/${install_dir}/boot/syslinux/syslinux.cfg

    cp ${work_dir}/root-image/usr/lib/syslinux/bios/*.c32 ${work_dir}/iso/${install_dir}/boot/syslinux/
    cp ${work_dir}/root-image/usr/lib/syslinux/bios/lpxelinux.0 ${work_dir}/iso/${install_dir}/boot/syslinux
    cp ${work_dir}/root-image/usr/lib/syslinux/bios/memdisk ${work_dir}/iso/${install_dir}/boot/syslinux

    mkdir -p ${work_dir}/iso/${install_dir}/boot/syslinux/hdt
    gzip -c -9 ${work_dir}/root-image/usr/share/hwdata/pci.ids > ${work_dir}/iso/${install_dir}/boot/syslinux/hdt/pciids.gz
    gzip -c -9 ${work_dir}/root-image/usr/lib/modules/*-ARCH/modules.alias > ${work_dir}/iso/${install_dir}/boot/syslinux/hdt/modalias.gz
}

# Prepare /isolinux
make_isolinux() {
    mkdir -p ${work_dir}/iso/isolinux
    sed "s|%INSTALL_DIR%|${install_dir}|g" ${script_path}/isolinux/isolinux.cfg > ${work_dir}/iso/isolinux/isolinux.cfg
    cp ${work_dir}/root-image/usr/lib/syslinux/bios/isolinux.bin ${work_dir}/iso/isolinux/
    cp ${work_dir}/root-image/usr/lib/syslinux/bios/isohdpfx.bin ${work_dir}/iso/isolinux/
    cp ${work_dir}/root-image/usr/lib/syslinux/bios/ldlinux.c32 ${work_dir}/iso/isolinux/
}

# Process aitab
make_aitab() {
    sed "s|%ARCH%|${arch}|g" ${script_path}/aitab > ${work_dir}/iso/${install_dir}/aitab
}

# Build all filesystem images specified in aitab (.fs.sfs .sfs)
make_prepare() {
    ${_pre} prepare
}

# Build ISO
make_iso() {
    ${_pre} checksum
    ${_pre} pkglist
    ${_pre} -L "${iso_label}" -o "${out_dir}" iso "${iso_name}-${iso_version}-${arch}.iso"
}


for arch in ${build_arch}; do
    work_dir=work/${arch}

    pacman_conf=${work_dir}/pacman.conf
    _pre="setarch ${arch} mkarchiso -v -w ${work_dir} -C ${pacman_conf} -D ${install_dir}"

    mkdir -p ${work_dir}

    run_once make_pacman_conf
    run_once make_basefs
    run_once make_packages
    run_once make_setup_mkinitcpio
    run_once make_customize_root_image
    run_once make_boot
    run_once make_syslinux
    run_once make_isolinux
    run_once make_aitab
    run_once make_prepare
    run_once make_iso
done
