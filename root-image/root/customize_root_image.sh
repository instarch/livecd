#!/bin/bash

set -e -u

_user=inst
DESKTOP=$1

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/

# Create our user if it doesn't already exist
set +e
if ! $(grep -q "^${_user}:" /etc/passwd); then
    useradd -m -p "$(openssl passwd arch)" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel,vboxsf" -s /usr/bin/zsh ${_user}
fi
set -e

cat > /home/${_user}/.xinitrc <<EOT
#!/bin/sh

if \$(grep -qi VBOX /proc/scsi/scsi); then
    /usr/bin/VBoxClient-all
fi

exec dbus-launch i3 > ~/.i3/i3.log 2>&1
EOT

systemctl enable vboxservice.service sshd.service

if [ -e /usr/lib/systemd/system/sddm.service ]; then
    systemctl enable sddm.service

    # update SDDM config
    sed -i -e "/^LastSession=/c LastSession=${DESKTOP}.desktop" \
           -e "/^CursorTheme=/c CursorTheme=jimmac" \
           -e "/^CurrentTheme=/c CurrentTheme=maldives" \
           -e "/^AutoUser=/c AutoUser=${_user}" \
           -e "/^LastUser=/c LastUser=${_user}" /etc/sddm.conf
fi

if [ -e /usr/lib/systemd/system/lightdm.service ]; then
    systemctl enable lightdm.service

    /usr/lib/lightdm/lightdm/lightdm-set-defaults -g lightdm-gtk-greeter -s ${DESKTOP} -a ${_user}

    # update LightDM config
    sed -i -e "/^#*xserver-command=/c xserver-command=X -dpi 96" \
           -e "/^#*autologin-user-timeout=/c autologin-user-timeout=0" /etc/lightdm/lightdm.conf
fi

chmod 750 /etc/sudoers.d
chmod 440 /etc/sudoers.d/g_wheel

sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

chown -R ${_user}:users /home/${_user}
chown -R root:root /etc/sudoers.d
