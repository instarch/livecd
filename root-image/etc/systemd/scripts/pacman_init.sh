#!/bin/bash

/usr/bin/pacman-key --init
/usr/bin/pacman-key --populate archlinux

# add my key to the keyring
/usr/bin/pacman-key -r 051680AC

# add infinality repo key
/usr/bin/pacman-key -r 962DDE58


/usr/bin/pacman-key --refresh-keys
/usr/bin/pacman-key --lsign-key 051680AC
/usr/bin/pacman-key --lsign-key 962DDE58

# synchronize databases
/usr/bin/pacman -Sy

# populate the command-not-found database
/usr/bin/cnf-sync
